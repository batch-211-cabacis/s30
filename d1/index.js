// AGGREGATION
/*

	AGGREGATE METHODS
	1.Match
	2. Group
	3. Sort
	4. Project

	AGGREGATION PIPELINES

	OPERATORS
	1.Sum
	2. Max
	3. Min
	4. Avg
*/

/*
	MONGODB AGGREGATION

	- used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
	- compared to doing CRUD operations on our data from our previous sessions, aggregation gives access to amnipultae, filter, ad compute for results, providing us with information to make necesssary development decisions.
	- aggregration in MongoDb are very flexible and you can form your own aggregation pipeline depending on the need of your application
*/

// AGGREGATE METHODS

/*
	1. MATCH - $match
	- is used to pass the documents that meet the specified conditions to the next pipeline stage/aggregation process
	SYNTAX
		{$match: {field: value}}
*/
db.fruits.aggregate([
	{$match: {onSale: true}}
	]);

db.fruits.aggregate([
 		{$match: {stock: {$gte: 20}}}
	]);

/*
	2. GROUP - $group
	- is used to group elements together field-value pairs using the data from the group element
	SYNTAX
		{$group: {_id: "value", fieldResult: "valueResult"}}
*/
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
	]);

/*
	3. SORT - $sort
	- can be used when changing the order of aggregated results
	- providing a value of -1 will sort the docuemnts in a descending order
	- providing a value of 1 will sort the documents will sort i ascending order
	SYNTAX
		{$sort: {field: 1/-1}}
*/
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$sort: {total: -1}}
	]);

/*
	4. Project - $project
	- can be used when aggregating data to include data to include or exclude fields from the returned results
	SYNTAX
		{$project: {field: 1/0}}
*/
db.fruits.aggregate([
	 	{$match: {onSale: true}},
	 	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	 	{$project: {_id: 0}}
	]);

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
		{$project: {_id: 0}},
		{$sort: {total: -1}}
	]);

/*
	AGGREGATION PIPELINE
	- aggregation pipeline in MongoDB is a framework for data aggregation
	- ecah stage transforms the documnets as they pass through the deadlines
	SYNTAX
		db.collectionName.aggregate([
			{stageA},
			{stageB},
			{stageC}
		]);
*/

// OPERATORS
/*
	1. $sum - gets the total of everything
*/
db.fruits.aggregate([
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
	]);

/*
	2. $max - gets the highest value out of everything else
*/
db.fruits.aggregate([
		{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
	]);

/*
	3. $min - gets lowest value out of evrything else
*/
db.fruits.aggregate([
		{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
	]);

/*
	4. $avg - gets the average value of all the fields
*/
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
	]);